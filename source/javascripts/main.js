;(function($) {

    $(document).ready(function () {
        //Slick slider
        $('#slider-intro').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            centerMode: true,
            infinite: true,
            speed: 700,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        $('#slider-round').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinite: true,
            speed: 500,
            nextArrow: '<i class="arrow-right"></i>',
            prevArrow: '<i class="arrow-left"></i>',
            autoplay: true,
            autoplaySpeed: 2000
        });

        //Tabs
        $(".tab_content").hide();
        $(".tab_content:first").show();

        /* if in tab mode */
        $("ul.tabs-list li").click(function() {

            $(".tab_content").hide();
            var activeTab = $(this).attr("rel");
            $("#"+activeTab).fadeIn();

            $("ul.tabs-list li").removeClass("active");
            $(this).addClass("active");

            $(".tab_drawer_heading").removeClass("d_active");
            $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

        });
        /* if in drawer mode */
        $(".tab_drawer_heading").click(function() {

            $(".tab_content").hide();
            var d_activeTab = $(this).attr("rel");
            $("#"+d_activeTab).fadeIn();

            $(".tab_drawer_heading").removeClass("d_active");
            $(this).addClass("d_active");

            $("ul.tabs-list li").removeClass("active");
            $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
        });


        //Responsive menu
        $('.toggle-nav').click(function(e) {
            $(this).toggleClass('active');
            $('.menu ul').toggleClass('active');

            e.preventDefault();
        });
    });
}(jQuery));


